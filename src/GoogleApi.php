<?php

namespace Uncgits\GoogleApi;

use Google_Client;
use Google_Service_Directory;

class GoogleApi
{

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */
    private $client;

    private $apiDelegate;

    private $proxyHost;
    private $proxyPort;

    private $useProxy = false;

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */


    /**
     * @param mixed $apiDelegate
     */
    public function setApiDelegate($apiDelegate)
    {
        $this->apiDelegate = $apiDelegate;
    }
    /**
     * @param mixed $proxyHost
     */
    public function setProxyHost($proxyHost)
    {
        $this->proxyHost = $proxyHost;
    }

    /**
     * @param mixed $proxyPort
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * @param boolean $useProxy
     */
    public function setUseProxy($useProxy)
    {
        $this->useProxy = ($useProxy === true);
    }


    public function getGoogleClient()
    {
        return $this->client;
    }

    /*
    |--------------------------------------------------------------------------
    | Authorized Client
    |--------------------------------------------------------------------------
    |
    |
    */

    public function googleClient()
    {
        $client = new Google_Client;
        $client->useApplicationDefaultCredentials();
        $client->setApplicationName("Google API");
        $client->setScopes([
            'https://www.googleapis.com/auth/calendar',
            'https://www.googleapis.com/auth/admin.directory.user',
            'https://www.googleapis.com/auth/admin.directory.group',
        ]);
        return $client;
    }


    /*
    |--------------------------------------------------------------------------
    | API Calls
    |--------------------------------------------------------------------------
    |
    |
    */
    /**
     * apiCall() - performs a call to the Google JSON REST API
     * https://www.googleapis.com/{{apiName}}/{{apiVersion}}/{{resourcePath}}?{{query}} {{string}}
     *
    /**
     * apiCall() - performs a call to the Google API
     *
     * @param string $function - API function (per Google API docs)
     * @param string $method - Request method (e.g. GET, POST, HEAD, etc.)
     * @param string $params - parameters for the API call (optional)
     * @param string $download - boolean, set to true if the result is a download
     *
     * @return array
     */

    protected function apiCall($function, $method, $params = [], $download = false)
    {

        //$config = config('google-api.auth');

        $client = self::googleClient();
        $client->setSubject($this->apiDelegate);

        // returns an authorized Guzzle HTTP Client
        $httpClient = $client->authorize();


        // assemble the request target and endpoint
        $endpoint = 'https://www.googleapis.com/' . $function;

        // set up basic options
        $requestOptions = [
            'http_errors' => false // don't throw exceptions on HTTP errors
        ];

        // params / body
        if (count($params) > 0 ) {
            if (strtolower($method) == 'get') {
                $requestOptions['query'] = $params;
            } else {
                $requestOptions['form_params'] = $params;
            }
        }

        // use proxy if it is set in .env
        if ($this->useProxy) {
            $requestOptions['proxy'] = $this->proxyHost . ':' . $this->proxyPort;
        }

        // perform the call
        $response = $httpClient->$method($endpoint, $requestOptions);

        // get response data
        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();
        $body = $response->getBody();
        $bodyContents = $body->getContents();

        // parse basic info for all requests

        // TODO - handle downloads.

        // json
        if (!$download) {
            $bodyContents = json_decode($bodyContents);
        }
        // return raw data about the request so that it can be parsed by the calling function

        return array(
            'response' => array(
                'endpoint' => $function,
                'method' => $method,
                'message'     => $servMessage,
                'httpCode' => $code,
                'httpReason' => $reason
            ),
            'body' => $bodyContents
        );

    }


    public function getAllGroups()
    {
        $config = config('google-api.notification_mode');
        $client = self::googleClient();
        $directory_service = new Google_Service_Directory($client);

        // $defaultParams = array('domain' => 'uncg.net','maxResults' =>100, 'orderBy' =>'email');
        $domain = config('google-api.domain');

        $groupList = $directory_service->groups->listGroups(array('domain' => $domain));
        $groups = $groupList->getGroups();
        return $groups;

    }



}